#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define N 80

typedef struct Info {
    char *str;
    struct Info* next;
} Info;

void printInfo(Info *info);

void removeInfo(Info *info);

typedef struct Node {
    char key[N];
    Info *info;
    struct Node *left;
    struct Node *right;
} Node;

Info *makeInfo(char *str);

Node* makeNode(char *key, Info *info);

void addToList(Node *node, Info *info);

Node* minEl(Node *node);

Node* maxEl(Node *node);

Node* insert(Node *node, char *key, Info *info);

Node* removeNode(Node* node, char *key);

Info* find(Node* node, char *key);

void printTree(Node* node, char *a, char *b);

//void printTreeInverse(Node* node, char *a, char *b);

void print(Node* node, int level);

Node* inputFromFile(FILE *file);

int myStrcmp(const char *str1, const char *str2);

Node* findMostDiff(Node *node, char *key);

void test(int n);

char* get_str(FILE *file);

int getInt();

int GetNum( const int low, const int high );

int CheckEOF();

char *getStr();
