#include "tree.h"

void printInfo(Info *info) {
    while (info) {
        printf("%s\n", info->str);
        info = info->next;
    }
}

void removeInfo(Info *info) {   //освобождение памяти
    Info *toDel = info;
    while (toDel) {
        info = info->next;
        free(toDel->str);
        free(toDel);
        toDel = info;
    }
}

Info *makeInfo(char *str) {         //создание информационной структуры
    Info *info = (Info*)malloc(sizeof(Info));
    info->str = (char*)calloc(strlen(str)+1, sizeof(char));
    strcpy(info->str, str);
    info->next = NULL;
    return info;
}

Node* makeNode(char *key, Info *info) {             //создание узла
    Node *node = malloc(sizeof(Node));
    node->left = node->right = NULL;
    strncpy(node->key, key, N);
    node->info = info;
    return node;
}

void addToList(Node *node, Info *info) {            //добавление элемента в список узла
    Info *last = node->info;
    while (last->next) {
        last = last->next;
    }
    last->next = info;
}

Node* minEl(Node *node) {   //определение мин по ключу элемента (самый левый потомок)
    while (node && node->left) {
        node = node->left;
    }
    return node;
}

Node* maxEl(Node *node) {     //определение макс по ключу элемента (самый правый потомок)
    while (node && node->right) {
        node = node->right;
    }
    return node;
}

Node* insert(Node *node, char *key, Info *info) {     //включение нового элемента в таблицу
    if (node == NULL) {
        node = makeNode(key, info);
    }
    else {
        int cmpResult = strcmp(key, node->key);
        if (cmpResult < 0) {
            node->left = insert(node->left, key, info);
        }
        else if (cmpResult > 0) {
            node->right = insert(node->right, key, info);
        }
        else {
            addToList(node, info);
        }
    }
    return node;
}

Node* removeNode(Node* node, char *key) {       //удаление элемента из таблицы
    if (node == NULL)
        return node;
    
    int cmpResult = strcmp(key, node->key);
    
    if (cmpResult < 0)
        node->left = removeNode(node->left, key);
    
    else if (cmpResult > 0)
        node->right = removeNode(node->right, key);
    
    else {
        if (node->info->next) {
            Info *toDel = node->info;
            node->info = node->info->next;
            free(toDel->str);
            free(toDel);
        }
        else {
            if (node->left == NULL) {
                Node *tmp = node->right;
                removeInfo(node->info);
                free(node);
                return tmp;
            }
            else if (node->right == NULL) {
                Node *tmp = node->left;
                removeInfo(node->info);
                free(node);
                return tmp;
            }
            Node* tmp = node->right;
            while (tmp && tmp->left != NULL) {
                tmp = tmp->left;
            }
            strncpy(node->key, tmp->key, N);
            node->info = tmp->info;
            node->right = removeNode(node->right, tmp->key);
        }
    }
    return node;
}

Info* find(Node* node, char *key) {       //поиск информации по заданному ключу
    if (node == NULL)
        return NULL;
    int cmpResult = strcmp(key, node->key);
    if(cmpResult == 0)
        return node->info;
    if(cmpResult < 0)
        return find(node->left, key);
    else
        return find(node->right, key);
}

Node* deleteTree(Node* node) {      //удаление дерева
    if(node != NULL) {
        deleteTree(node->left);
        deleteTree(node->right);
        removeInfo(node->info);
        free(node);
        node = NULL;
    }
    return NULL;
}

void printTree(Node* node, char *a, char *b) {  //обход в прямом порядке в указанном диапазоне
    if (node != NULL) {
        int cmpA = strcmp(node->key, a);
        int cmpB = strcmp(node->key, b);
        if (cmpA > 0) {
            printTree(node->left, a, b);
        }
        if (cmpA > 0 && cmpB < 0) {
            printf("%s\n", node->key);
            printInfo(node->info);
        }
        if (cmpB < 0) {
            printTree(node->right, a, b);
        }
    }
}


void print(Node* node, int level) {
    if(node) {
        print(node->right, level+5);
        for(int i = 0; i < level; i++)
            printf(" ");
        printf("%s\n", node->key);
        print(node->left, level+5);
    }
}

Node* inputFromFile(FILE *file) {   //ввод дерева из файла
    Node *root = NULL;
    char key[N];
    char *str;
    while (fgets(key, N, file)) {
        if (key[strlen(key)-1] == '\n')
            key[strlen(key)-1] = '\0';
        //fscanf(file, "%s " , str);
        str = get_str(file);
        Info *info = makeInfo(str);
        root = insert(root, key, info);
        free(str);
    }
    return root;
}


Node* findMostDiff(Node *node, char *key) {
    if (!node) {
        return NULL;
    }
    Node *max = maxEl(node);
    Node *min = minEl(node);
    if (strcmp(key, min->key) <= 0) {
        return max;
    }
    else if (strcmp(key, max->key) >= 0) {
        return min;
    }
    else if (strcmp(key, max->key) > strcmp(min->key, key)) {
        //printf("%d %d\n", myStrcmp(key, max->key), myStrcmp(min->key, key));
        return max;
    }
    else {
        //printf("%d %d\n", myStrcmp(key, max->key), myStrcmp(min->key, key));
        return min;
    }
}

void test(int n) {
    Node *root = NULL;
    char key[N];
    double avgTime = 0;
    for (int i = 0; i < n; i++) {
        sprintf(key, "%020d", i);
        Info *info = makeInfo("test");
        clock_t t = clock();
        insert(root, key, info);
        t = clock() - t;
        avgTime += (double)t/CLOCKS_PER_SEC * 1000;
    }
    printf("Общее время вставки элементов: %lf мс\n", avgTime);
    avgTime /= n;
    printf("Среднее время вставки элемента: %lf мс\n", avgTime);
    avgTime = 0;
    for (int i = 0; i < n; i++) {
        sprintf(key, "%020d", i);
        clock_t t = clock();
        find(root, key);
        t = clock() - t;
        avgTime += (double)t/CLOCKS_PER_SEC * 1000;
    }
    printf("Общее время поиска элементов: %lf мс\n", avgTime);
    avgTime /= n;
    printf("Среднее время поиска элемента: %lf мс\n", avgTime);
    avgTime = 0;
    for (int i = 0; i < n; i++) {
        sprintf(key, "%020d", i);
        clock_t t = clock();
        removeNode(root, key);
        t = clock() - t;
        avgTime += (double)t/CLOCKS_PER_SEC * 1000;
    }
    printf("Общее время удаления элементов: %lf мс\n", avgTime);
    avgTime /= n;
    printf("Среднее время удаления элемента: %lf мс\n", avgTime);
    deleteTree(root);
}

char* get_str(FILE *file) {             //ввод строки произвольной длины (с клавиаутры или из файла)
    char *str = NULL;
    char buf[81];
    size_t size = 0;
    int res;
    do {
        res = fscanf(file, "%80[^\n]", buf);
        if (res == 0) {
            fscanf(file, "%*c");
            if (!str) {
                str = malloc(sizeof(char));
                str[0] = '\0';
            }
        } else if (res > 0) {
            size_t len = strlen(buf);
            str = realloc(str, (size+len+1)*sizeof(char));
            memcpy(str+size, buf, len);
            size += len;
            str[size] = '\0';
        }
    } while (res > 0);
    return str;
}

int getInt() {      //ввод целого
    int number;
    int result;
    do {
        result = scanf("%d", &number);
        scanf("%*[^\n]s");
        scanf("%*c");
    } while (result != 1);
    return number;
}

int GetNum( const int low, const int high ) {
	char *p, s[ 100 ]; // указатель на символ и буфер для ввода
	long num; // для преобразования ввода в целое значение

	while ( 1 ) { // считываем ввод пользователя в цикле
		if ( CheckEOF() ) {
			puts( "EOF detected! Exit." );
			exit( 0 );
		}
		fgets( s, sizeof( s ), stdin );
		num = strtol( s, &p, 10 ); // пробуем преобразовать в целое
		// прверяем ввод пользователя и если введено

		if ( p == s || *p != '\n' || num < low || num > high ) { // НЕ целое число из заданного диапазона

			printf( "Введите число от %d до %d: ", low, high );

		} else
			break; // обрываем цикл ввода
	}
//
	return num; // возвращаем целое число
}

int CheckEOF() {
	if ( feof( stdin ) ) //если конец файла
		return 1;
	int c = getc( stdin ); //считывает элементы
	if ( c == EOF )
		return 1;
	ungetc( c, stdin );
	return 0;
}

char *getStr() {
	char buf[81] = {0};
	char *res = NULL;
	char *check = NULL;
	int len = 0;
	int n = 0;

	do {
		n = scanf("%80[^\n]", buf);
		if (n < 0) {
			if (!res) {
				return NULL;
			}
		} else if (n > 0) {
			int chunk_len = strlen(buf);
		    int str_len = len + chunk_len;
		    check = res;
		    res = realloc(res, str_len + 1);
		    if (!res) {
		    	free(check);
		    	return NULL;
		    }
		    memcpy(res + len, buf, chunk_len);
		    len = str_len;
		} else {
			scanf("%*c");
		}
	} while (n > 0);

	if (len > 0) {
		res[len] = '\0';
	} else {
		res = calloc(1, sizeof(char));
	}
	return res;
}
