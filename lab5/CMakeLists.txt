cmake_minimum_required(VERSION 3.19)
project(5 C)

set(CMAKE_C_STANDARD 99)

add_executable(5 main.c graph.c graph.h)
target_link_libraries(5 m)
