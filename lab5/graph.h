#ifndef INC_5_GRAPH_H

struct GraphVertex;

typedef struct GraphEdge {
    struct GraphVertex* to;
    double weight;
    struct GraphEdge* next;
} Edge;

typedef struct GraphVertex {
    char* name;
    int real, imag; // real + i * imag
    int number; // номер вершины для операций на графе; чтобы ассоциировать индекс в массах с вершиной
    struct GraphVertex* next_vertex;
    Edge* edges;
} Vertex;

struct GlobalGraph {
    Vertex* first;
    Vertex* last;
    int n;
};

typedef struct Stack {
    int *mass;
    int size;
} stack;

void add_edge(Vertex* from, Vertex* to);
void add_vertex(Vertex* v);
long long generate_new_graph(int n, int edges, Vertex*** vertices_ret);
void delete_graph();
void print_vertex(Vertex* v);
void print_graph();
Vertex** BFS(Vertex* src, Vertex* dest, int* path_n);
stack *Dijkstra(Vertex *from, Vertex *to);
int stack_delete(stack *stack);
Vertex *find_by_number(int number);

#define INC_5_GRAPH_H
#endif 
