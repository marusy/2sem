
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stdint.h>
#include <time.h>
#include "graph.h"

#define xread(var) fread(&(var), sizeof(var), 1, file)
#define xwrite(var) fwrite(&(var), sizeof(var), 1, file)

int get_int() {
    int x;
    int n;
    do {
        n = scanf("%d", &x);
        if (n < 0) {    //EOF
            exit(EOF);
        }
        if (n == 0) {
            printf("Error! Repeat input!\n");
            scanf("%*[^\n]");
        }
    } while (n == 0);
    scanf("%*c");
    return x;
}

char *get_str() {
    char buf[81];
    char *res = NULL;
    unsigned int len = 0;
    int n;

    do {
        n = scanf("%80[^\n]", buf);
        if (n < 0) {
            free(res);
            exit(EOF);
        } else if (n > 0) {
            unsigned int new_len = len + strlen(buf);
            res = realloc(res, new_len + 1);
            memcpy(res + len, buf, strlen(buf));
            len = new_len;
        } else {
            scanf("%*c");
        }
    } while (n > 0);

    if (len > 0) {
        res[len] = '\0';
    } else {
        res = (char *) calloc(1, sizeof(char));
    }
    return res;
}


char *get_name() {
    static char s[201];
    if (scanf("%201s", s) == EOF)
        exit(0);
    return strdup(s);
}

extern struct GlobalGraph graph;

void D_add_vertex() {
    Vertex *vertex = malloc(sizeof(Vertex));
    vertex->name = get_name();
    vertex->real = get_int();
    vertex->imag = get_int();
    vertex->edges = NULL;
    vertex->next_vertex = NULL;
    add_vertex(vertex);
}

void D_add_edge() {
    printf("from: ");
    char *name1 = get_name();
    printf("to: ");
    char *name2 = get_name();
    Vertex *v1 = NULL;
    Vertex *v2 = NULL;
    for (Vertex *v = graph.first; v != NULL; v = v->next_vertex) {
        if (strcmp(v->name, name1) == 0)
            v1 = v;
        if (strcmp(v->name, name2) == 0)
            v2 = v;
    }
    if (v1 == NULL || v2 == NULL) {
        printf("Point \"%s\" not found", v1 == NULL ? name1 : name2);
    } else {
        add_edge(v1, v2);
    }
}

void D_delete_vertex() {
    printf("name: ");
    char *name = get_name();
    Vertex **next = &graph.first;
    while (*next != NULL) { //поиск этой вершины
        if (strcmp((*next)->name, name) == 0)
            break;
        next = &(*next)->next_vertex;
    }
    if (*next == NULL) {
        printf("Point \"%s\" not found", name);
        return;
    }
    graph.n -= 1;
    Vertex *found = *next;
    *next = found->next_vertex;
    for (Vertex *node = graph.first; node != NULL; node = node->next_vertex) { //уменьшение номеров вершин
        if (node->number >= found->number)
            node->number -= 1;
        for (Edge **edge = &node->edges; *edge != NULL; edge = &(*edge)->next) { //удаление ребер указывающих на нее
            if ((*edge)->to == found) {
                Edge *tmp = *edge;
                *edge = tmp->next;
                free(tmp);
                break;
            }
        }
    }
    free(found->name);
    Edge *edge = found->edges;
    while (edge != NULL) { //удаление всех ребер вершины
        Edge *tmp = edge;
        edge = edge->next;
        free(tmp);
    }
    free(found); //удаление самой вершины
}

void D_delete_edge() {
    printf("from: ");
    char *name1 = get_name();
    printf("to: ");
    char *name2 = get_name();
    Vertex *v1 = NULL;
    Vertex *v2 = NULL;
    for (Vertex *v = graph.first; v != NULL; v = v->next_vertex) {
        if (strcmp(v->name, name1) == 0)
            v1 = v;
        if (strcmp(v->name, name2) == 0)
            v2 = v;
    }
    if (v1 == NULL || v2 == NULL) {
        printf("Point \"%s\" not found", v1 == NULL ? name1 : name2);
    } else {
        for (Edge **edge = &v1->edges; *edge != NULL; edge = &(*edge)->next) { //удаление ребра
            if ((*edge)->to == v2) {
                Edge *tmp = *edge;
                *edge = tmp->next;
                free(tmp);
                break;
            }
        }
        printf("Removed\n");
    }
}

char *read_str(FILE *file) {
    int len;
    xread(len);
    if (len == 0)
        return NULL;
    char *res = malloc(len + 1);
    fread(res, len, 1, file);
    res[len] = 0;
    return res;
}

void write_str(FILE *file, char *string) {
    if (string == NULL) {
        int len = 0;
        xwrite(len);
        return;
    }
    unsigned int len = strlen(string);
    xwrite(len);
    fwrite(string, len, 1, file);
}

void open_file(char *name) {
    FILE *file = NULL;
    file = fopen(name, "rb+");
    if (file == NULL) {
        file = fopen(name, "wb+");
        if (file == NULL) {
            printf("Error creating file: %s\n", strerror(1));
            exit(5);
        }
        fclose(file);
        printf("Created new file\n");
        printf("Generate new graph? (y/n) ");
        {
            char *ans = get_str();
            if (ans && ans[0] != 'y') {
                free(name);
                return;
            }
            free(ans);
        }
        srand(3);
        printf("number of Nodes: ");
        int n = get_int();
        if (n <= 1) {
            printf("false");
            exit(0);
        }
        printf("edges: ");
        int edges = get_int();
        if (edges <= 0) {
            printf("false");
            exit(0);
        }
        generate_new_graph(n, edges, NULL);
        return;
    }
    int n;
    xread(n);
    graph.n = n;
    if (n <= 0)
        return;
    Vertex **vertices = malloc(n * sizeof(Vertex *));

    for (int i = 0; i < n; ++i) {
        Vertex *v = vertices[i] = malloc(sizeof(Vertex));
        v->next_vertex = NULL;
        if (i > 0)
            vertices[i - 1]->next_vertex = v;
        v->number = i;
        v->name = read_str(file);
        xread(v->real);
        xread(v->imag);
        int edges_num;
        xread(edges_num);
        v->edges = NULL;
        Edge **cur = &v->edges;
        for (int j = 0; j < edges_num; ++j) {
            int v_link;
            double weight;
            xread(v_link);
            xread(weight);
            Edge *edge = malloc(sizeof(Edge));
            // сохраняем индекс вершины в поле указателя на вершину чтобы не создавать новую память для этого
            *edge = (Edge) {.to=(Vertex *) (uintptr_t) v_link, .weight=weight, .next=NULL};
            *cur = edge;
            cur = &edge->next;
        }
    }
    graph.first = vertices[0];
    graph.last = vertices[n - 1];

    for (int i = 0; i < n; ++i) {
        for (Edge *edge = vertices[i]->edges; edge != NULL; edge = edge->next) {
            // меняем сохраненные индексы вершин на указатели на вершины
            edge->to = vertices[(uintptr_t) edge->to];
        }
    }
    free(vertices);
}

void close_file(char *name) {
    FILE *file = fopen(name, "wb");
    if (file == NULL) {
        printf("Error creating file\n");
        exit(5);
    }
    xwrite(graph.n);

    for (Vertex *v = graph.first; v != NULL; v = v->next_vertex) {
        write_str(file, v->name);
        free(v->name);
        xwrite(v->real);
        xwrite(v->imag);
        int edges_num = 0;
        for (Edge *edge = v->edges; edge != NULL; edge = edge->next) {
            edges_num += 1;
        }
        xwrite(edges_num);
        while (v->edges != NULL) {
            Edge *edge = v->edges;
            xwrite(edge->to->number);
            xwrite(edge->weight);
            v->edges = edge->next;
            free(edge);
        }
    }
    Vertex *v = graph.first;
    while (v != NULL) {
        Vertex *next = v->next_vertex;
        free(v);
        v = next;
    }
    fclose(file);
}

void D_find_path_bfs() { //поиск в ширину
    printf("from: ");
    char *name1 = get_name();
    printf("to: ");
    char *name2 = get_name();
    Vertex *v1 = NULL;
    Vertex *v2 = NULL;
    for (Vertex *v = graph.first; v != NULL; v = v->next_vertex) {
        if (strcmp(v->name, name1) == 0)
            v1 = v;
        if (strcmp(v->name, name2) == 0)
            v2 = v;
        if (v1 && v2)
            break;
    }
    if (v1 == NULL || v2 == NULL) {
        printf("Point \"%s\" not found", v1 == NULL ? name1 : name2);
        return;
    }
    int n;
    Vertex **vertices = BFS(v1, v2, &n);
    if (vertices == NULL) {
        printf("No such path");
        free(name1);
        free(name2);
        return;
    }
    printf("%s", vertices[0]->name);
    for (int i = 1; i < n; ++i) {
        printf(" -> %s", vertices[i]->name);
    }
    free(vertices);
    free(name1);
    free(name2);
}

void D_timing() {
    printf("Num of tests:");
    int num = get_int();
    delete_graph();
    clock_t first, run_time;
    int i = 0;
    int nv = 0, ne = 3;
    int t, k = 0;
    srand(time(0));
    for (t = 0; t < num; t++) {
        if (t % 40 == 0)
            ne += 1;
        nv += 25;
        Vertex **nodes;
        long long edges = generate_new_graph(nv, ne, &nodes);
        i = 0;
        k = 0;
        first = clock();
        while (i < nv) {
            int a = rand() % nv;
            int b = rand() % nv;
            stack *stack = Dijkstra(nodes[a], nodes[b]);
            if (stack) {
                k += 1;
                free(stack->mass);
                free(stack);
            }
            i++;
        }
        run_time = clock() - first;
        printf("Тест #%d: %d вершин, %lld рёбер, найдено %d/%d путей, время: %ld\n", t + 1, nv, edges, k, nv, run_time);
        float time1 = (float) run_time / (float) nv;
        long long VE = nv * edges;
        printf("    время/n = %.2f, VE = %lld, t1/VE = %.2f * 10^-6\n", time1, VE, time1 / (float) VE * 1000000);
        delete_graph();
    }
}

void D_Dijkstra() {
    printf("from: ");
    char *name1 = get_name();
    printf("to: ");
    char *name2 = get_name();
    Vertex *v1 = NULL;
    Vertex *v2 = NULL;
    for (Vertex *v = graph.first; v != NULL; v = v->next_vertex) {
        if (strcmp(v->name, name1) == 0)
            v1 = v;
        if (strcmp(v->name, name2) == 0)
            v2 = v;
        if (v1 && v2)
            break;
    }
    if (v1 == NULL || v2 == NULL) {
        printf("Point \"%s\" not found", v1 == NULL ? name1 : name2);
        return;
    }
    stack *stack = Dijkstra(v1, v2);
    if (!stack) {
        printf("No such path\n");
        free(name1);
        free(name2);
        return;
    }
    printf("path: ");
    while (stack->size != 1) {
        Vertex *v = find_by_number(stack_delete(stack));
        printf("%s ->", v->name);
    }
    Vertex *v = find_by_number(stack_delete(stack));
    printf("%s", v->name);
    printf("\n");
    free(stack->mass);
    free(stack);
    free(name1);
    free(name2);
}

int main() {
    setbuf(stdout, NULL);
    printf("Enter file name: ");
    char *name = get_str();
    open_file(name);
    int choice = 1;
    while (choice != 0) {
        printf("0. Exit\n1. Add point\n2. Add edge\n3. Delete point\n4. Delete edge\n"
               "5. Print graph\n6. BFS\n7. Dijkstra\n8. Timing\n->");
        choice = get_int();
        if (choice < 0 || choice > 9) {
            printf("no\n");
            continue;
        }
        switch (choice) {
            case 1:
                D_add_vertex();
                break;
            case 2:
                D_add_edge();
                break;
            case 3:
                D_delete_vertex();
                break;
            case 4:
                D_delete_edge();
                break;
            case 5:
                print_graph();
                break;
            case 6:
                D_find_path_bfs();
                break;
            case 7:
                D_Dijkstra();
                break;
            case 8:
                D_timing();
                break;
            default:;
        }
        printf("\n");
    }
    close_file(name);
    free(name);
    return 0;
}
