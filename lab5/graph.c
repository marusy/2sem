#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include "graph.h"

struct GlobalGraph graph = {NULL, NULL, 0}; // глобальная переменная чтобы не передавать её в каждую функцию

double dist(Vertex *a, Vertex *b) {// функция расстояния (нахождение гипотенузы треугольника)
    return hypot(a->real - b->real, a->imag - b->imag);
}

void add_edge(Vertex *from, Vertex *to) {//добавление новой связи между вершинами
    Edge *edge = malloc(sizeof(Edge));
    *edge = (Edge) {.to=to, .weight=dist(from,
                                         to), .next=from->edges}; //добавление связи между вершинами в список связей у вершины
    from->edges = edge;
}

void add_vertex(Vertex *v) {//добавление вершины
    graph.n += 1; //увеличение количества вершин всего
    if (graph.first == NULL) { //если вершин в графе нет
        v->number = 0;
        graph.first = graph.last = v;
    } else { //добавление вершины в списко всех вершин
        v->number = graph.last->number + 1;
        graph.last->next_vertex = v;
        graph.last = v;
    }
}

long long generate_new_graph(int n, int edges, Vertex ***vertices_ret) {//генерация рандомного графа
    long long total_edges = 0;
    Vertex **vertices = malloc(n * sizeof(Vertex *));
    for (int i = 0; i < n; ++i) {
        Vertex *v = vertices[i] = malloc(sizeof(Vertex));
        if (i > 0)
            vertices[i - 1]->next_vertex = vertices[i];
        v->number = i;
        v->name = malloc(26);
        snprintf(v->name, 26, "v%d", i + 1);
        v->edges = NULL;
        gen_coords:
        v->real = rand() % (i + 5);
        v->imag = rand() % (i + 5);
        for (int j = 0; j < i; ++j) {
            if (v->real == vertices[j]->real && v->imag == vertices[j]->imag) {
                goto gen_coords;
            }
        }
        add_vertex(v);
    }
    graph.first = vertices[0];
    graph.last = vertices[n - 1];
    graph.last->next_vertex = NULL;
    bool *connection = calloc(n, 1);
    for (int i = 0; i < n; ++i) {
        Vertex *v = vertices[i];
        int n_edges;
        if (rand() % 3 == 0)
            n_edges = edges + (rand() % (edges * 2));
        else
            n_edges = edges - (rand() % (edges / 2 + 1));
        if (n_edges > n - 1)
            n_edges = n - 1;
        total_edges += n_edges;
        connection[i] = true;
        for (int j = 0; j < n_edges; ++j) {
            int index = i;
            while (connection[index] == true) {
                index = rand() % n;
            }
            connection[index] = true;
            add_edge(v, vertices[index]);
        }

        for (Edge *edge = v->edges; edge != NULL; edge = edge->next) {
            connection[edge->to->number] = false;
        }
        connection[i] = false;
    }
    free(connection);
    if (vertices_ret == NULL)
        free(vertices);
    else
        *vertices_ret = vertices;
    return total_edges;
}

void delete_graph() {//удаление графа, проход по всем вершинам графа через список всех вершин и их очищение
    while (graph.first != NULL) {
        Vertex *node = graph.first;
        free(node->name);
        while (node->edges != NULL) {
            Edge *edge = node->edges;
            node->edges = edge->next;
            free(edge);
        }
        graph.first = node->next_vertex;
        free(node);
    }
    graph.first = NULL;
    graph.last = NULL;
    graph.n = 0;
}

void print_vertex(Vertex *v) {
    printf("%s (%d, %d) ->", v->name, v->real, v->imag);
    for (Edge *edge = v->edges; edge != NULL; edge = edge->next) {
        printf(" %s(%lf)", edge->to->name, edge->weight);
    }
}

void print_graph() {
    for (Vertex *v = graph.first; v != NULL; v = v->next_vertex) {
        print_vertex(v);
        printf("\n");
    }
}

Vertex **BFS(Vertex *src, Vertex *dest, int *path_n) {// поиск в ширину
    bool *visited = calloc(graph.n, sizeof(bool));
    visited[src->number] = true;
    Vertex **from = calloc(graph.n, sizeof(Vertex *)); // информация о пути; из какой вершины можно прийти в данную
    Vertex **queue = malloc(graph.n * sizeof(Vertex *));
    int len = 1;
    int start = 0;
    queue[0] = src;
    while (len > 0) {
        Vertex *from_v = queue[start];
        if (from_v == dest)
            break;
        start += 1;
        len -= 1;
        for (Edge *edge = from_v->edges; edge != NULL; edge = edge->next) {
            Vertex *v = edge->to;
            if (visited[v->number])
                continue;
            visited[v->number] = true;
            from[v->number] = from_v;
            queue[start + len] = v;
            len += 1;
        }
    }
    if (from[dest->number] == NULL) {
        free(from);
        free(visited);
        return NULL;
    }
    // создание массива содержащего пусть от src до dest
    len = 1;
    Vertex *cur = dest;
    while (cur != src) {
        len += 1;
        cur = from[cur->number];
    }
    *path_n = len;
    Vertex **res = malloc(len * sizeof(Vertex *));
    cur = dest;
    len -= 1;
    res[len] = cur;
    while (len > 0) {
        len -= 1;
        cur = res[len] = from[cur->number];
    }
    free(from);
    free(visited);
    return res;
}

int get_num_of_edges(Vertex *v) {
    Edge *e = v->edges;
    int i = 0;
    while (e) {
        ++i;
        e = e->next;
    }
    return i;
}

Vertex *find_by_number(int number) {
    Vertex *res = graph.first;
    for (int i = 0; i < graph.n; ++i) {
        if (res->number == number) return res;
        res = res->next_vertex;
    }
    return NULL;
}

Vertex *min_path(const double *paths, const bool *visited) {
    double minimum = INFINITY;
    int index = -1;
    for (int i = 0; i < graph.n; ++i) {
        if (paths[i] < minimum && visited[i] == 0) minimum = paths[i];
    }
    for (int i = 0; i < graph.n; ++i) {
        if (paths[i] == minimum && visited[i] == 0) {
            index = i;
            break;
        }
    }
    if (index == -1) return NULL;
    return find_by_number(index);
}

void stack_add(stack *stack, int inserting) {
    stack->mass = realloc(stack->mass, sizeof(int) * ++stack->size);
    stack->mass[stack->size - 1] = inserting;
}

int stack_delete(stack *stack) {
    int deleting = stack->mass[--stack->size];
    stack->mass = realloc(stack->mass, stack->size * sizeof(int));
    return deleting;
}

stack *Dijkstra(Vertex *from, Vertex *to) {
    double *paths = calloc(graph.n, sizeof(double ));
    bool *visited = calloc(graph.n, sizeof(bool));
    int *parents = calloc(graph.n, sizeof(int));
    for (int i = 0; i < graph.n; ++i) {
        paths[i] = INFINITY;
        visited[i] = 0;
        parents[i] = 0;
    }
    Vertex *current = from;
    paths[from->number] = 0;
    Edge *e;
    while (current) {
        int num_of_edges = get_num_of_edges(current);
        e = current->edges;
        for (int i = 0; i < num_of_edges; ++i) {
            if (!visited[e->to->number] && paths[current->number] + e->weight < paths[e->to->number]) {
                paths[e->to->number] = paths[current->number] + e->weight;
                parents[e->to->number] = current->number;
            }
            e = e->next;
        }
        visited[current->number] = true;
        current = min_path(paths, visited);
    }
    stack *result = malloc(sizeof(stack));
    result->mass = malloc(1);
    result->size = 0;
    for (int i = to->number; i != from->number; i = parents[i]) {
        if (i == parents[i]) {
            free(paths);
            free(visited);
            free(parents);
            free(result->mass);
            free(result);
            return NULL;
        }
        stack_add(result, i);
    }
    stack_add(result, from->number);
    free(paths);
    free(visited);
    free(parents);
    return result;
}
