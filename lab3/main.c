#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include "lib.h"



Table* open_file() {
    printf("Введите имя файла:\n");
    char* fname = getStr();
    if (fname == NULL || fname[0] == 0)
        return 0;
    Table* ptr = load_table(fname);
    if (ptr == NULL) {
        ptr = create_table();
        ptr->fd = fopen(fname, "w+b");
        fwrite(&ptr->csize1, sizeof(int), 1, ptr->fd); // записать один int
        ptr->info_end = sizeof(int);
    }
    free(fname);
    return ptr;
}

int main() {
    setbuf(stdout, NULL);
    SetConsoleOutputCP(CP_UTF8);
	int answer;
	Table *table = open_file();
    if (table == NULL)
        return 1;
	do {
		answer = dialog(0);
		switch(answer) {
			case 1:
				write_to_table(table);
				break;
			case 2:
				if (search(table)) {answer = 5;};
				break;
			case 3:
				remove_item(table);
				break;
			case 4:
				show_table(table);
				break;
			case 5:
				
				break;
		}
	} while (answer != 5);
    save_table(table);
	delete_table(table);
	return 0;
}
